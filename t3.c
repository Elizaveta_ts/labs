#include <stdio.h>
#define _USE_MATH_DEFINES
#include <math.h>

int main()
{
  float number;
  char format=0;

printf("Enter angle, please: ");
scanf( "%f%c", &number, &format);

if (format == 'R')
{
	number=number*180/M_PI;
	printf("%.2f%c\n", number, 'D');
}

else if (format =='D')
{
	number=number*M_PI/180;
	printf("%.2f%c\n", number, 'R');
}
else
	printf  ("Error! Format must be only D or R. Try again!");
return 0;


}