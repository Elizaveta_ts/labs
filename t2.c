#include <stdio.h>
int main()
{
  int hr=0, mn=0, sec=0;
    printf ("Enter time \n");
	scanf("%d:%d:%d", &hr, &mn, &sec);
    if ( hr>0 && hr < 24) //set limits for day-24 hours
    {

       hr = hr%24;
        if(hr < 12 && hr>=5)
           printf("Good morning\n");
        else if(hr >= 12 && hr <= 23)
        printf("Good afternoon\n");
        else if(hr >= 24 && hr <= 4)
            printf("Good night\n");
     }

	if (hr > 24) 
	{
		printf("Invalid time\n");
	}

    return 0;
}